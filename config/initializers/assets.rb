# Be sure to restart your server when you modify this file.

# Version of your assets, change this if you want to expire all your assets.
Rails.application.config.assets.version = '1.0'

# Add additional assets to the asset load path
# Rails.application.config.assets.paths << Emoji.images_path

# Precompile additional assets.
# application.js, application.css, and all non-JS/CSS in app/assets folder are already added.
# Rails.application.config.assets.precompile += %w( search.js )
Rails.application.config.assets.precompile += %w( welcome.js )
Rails.application.config.assets.precompile += %w( welcome.css )
Rails.application.config.assets.precompile += %w( alt.js )
Rails.application.config.assets.precompile += %w( alt.css )
Rails.application.config.assets.precompile += %w( christmas/welcome.js )
Rails.application.config.assets.precompile += %w( christmas/welcome.css )
Rails.application.config.assets.precompile += %w( videojs-contrib-hls.js )
Rails.application.config.assets.precompile += %w( winter/welcome.css )
Rails.application.config.assets.precompile += %w( winter/welcome.js )
Rails.application.config.assets.precompile += %w( spring/welcome.css )
Rails.application.config.assets.precompile += %w( spring/welcome.js )
