Rails.application.routes.draw do
  resources :shows do
    resources :air_dates
  end
  resources :alt do
    collection do
      get 'video'
      get 'chat'
    end
  end

  root 'welcome#index'
  get 'video', to: 'welcome#video'
  get 'chat', to: 'welcome#chat'
  get 'viewers', to: 'viewer_count#index'
  resources :chat

  mount ActionCable.server, at: '/cable'
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
