class AirDate < ApplicationRecord
  belongs_to :show

  validates :episode, presence: true
end
