class Viewer < ApplicationRecord
  validates :ip, presence: true,
            uniqueness: { case_sensitive: false }

end
