class User < ApplicationRecord
  validates :username, presence: true,
            uniqueness: { case_sensitive: false }
  validates :hex_id, presence: true,
            uniqueness: true
end
