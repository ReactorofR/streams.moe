class Message < ApplicationRecord
  belongs_to :room

  validates :user, presence: true
  validates :text, presence: true
end
