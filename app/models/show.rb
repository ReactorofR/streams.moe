class Show < ApplicationRecord
  has_many :air_dates
  validates :title, presence: true
end
