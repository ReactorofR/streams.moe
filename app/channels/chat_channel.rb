class ChatChannel < ApplicationCable::Channel
  def subscribed
    stream_from "chat"
  end

  def unsubscribed
    # Any cleanup needed when channel is unsubscribed
  end

  def speak(data)
    message = Message.create! room_id: data['room'], text: data['text'], user: data['user']
    if data['mentions']
      data['mentions'].each do |mention|
        user = User.find_by(username: mention)
        if user
          ActionCable.server.broadcast "user_channel_#{user.hex_id}",type: 'mention', mentioner: data['user'], text: data['text']
        end
      end
    end
    ActionCable.server.broadcast 'chat', message: message, type: 'message'
  end
end
