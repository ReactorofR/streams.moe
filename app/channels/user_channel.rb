class UserChannel < ApplicationCable::Channel
  def subscribed
    # stream_from "some_channel"
    @user_id = SecureRandom.hex(10)
    @user_channel = "user_channel_#{@user_id}"
    stream_from @user_channel
    messages = Message.where('room_id = ?',params[:room_id]).limit(50).order(created_at: :desc).reverse
    ActionCable.server.broadcast @user_channel, messages: messages, type: 'messages'
  end

  def unsubscribed
    if @user_name != nil
      ActionCable.server.broadcast 'chat', text:"User #{@user_name} has disconnected", type: 'info'
    end
    # Any cleanup needed when channel is unsubscribed
    users = User.where('online = :true and  updated_at >= :recent',{true: true,recent: Time.now - 60.seconds}).select(:username).load
    ActionCable.server.broadcast 'chat', users: users.to_a, type: 'users'
  end

  def anounce(data)
    if @user_name != nil
      ActionCable.server.broadcast 'chat', text:"User #{@user_name} is now known as #{data['user_name']}", type: 'info'
      @user_name = data['user_name']
      @user.update(username: @user_name)
    else
      @user_name = data['user_name']
      @user = User.find_by(username: @user_name)
      if not @user
        User.create! username: @user_name, hex_id: @user_id, online: true
      else
        @user.update({hex_id: @user_id, online: true})
      end
      ActionCable.server.broadcast 'chat', text:"User #{data['user_name']} connected", type: 'info'
    end
    if User.where(online: true).select(:username).any?
      users = User.where('online = :true and  updated_at >= :recent',{true: true,recent: Time.now - 60.seconds}).select(:username).load
      ActionCable.server.broadcast 'chat', users: users.to_a, type: 'users'
    end
  end

  def keep_online
    if @user
      @user.update({updated_at: Time.now, online: true})
    end
    users = User.where('online = :true and  updated_at >= :recent',{true: true,recent: Time.now - 60.seconds}).select(:username).load
    ActionCable.server.broadcast @user_channel, users: users.to_a, type: 'users'
  end

end
