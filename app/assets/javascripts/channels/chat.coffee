App.chat = App.cable.subscriptions.create "ChatChannel",
  connected: ->
  # Called when the subscription is ready for use on the server
    console.info('Connected to ChatChannel')
  disconnected: ->
    # Called when the subscription has been terminated by the server

  received: (data) ->
    # Called when there's incoming data on the websocket for this channel
    chat.parseRecieved data
  speak : (data) ->
    @perform 'speak', data
