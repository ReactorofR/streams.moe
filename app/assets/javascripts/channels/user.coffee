App.user = App.cable.subscriptions.create {channel: "UserChannel", room_id: 1},
  connected: ->
    console.info('Connected to UserChannel')
    @keepOnline()
    # Called when the subscription is ready for use on the server

  disconnected: ->
    # Called when the subscription has been terminated by the server

  received: (data) ->
    chat.parseRecieved(data)
    # Called when there's incoming data on the websocket for this channel

  anounce: (name) ->
    @perform  'anounce', {user_name: name}

  keepOnline: ->
    setInterval @keepOnline,60000
    App.user.perform 'keep_online', {online: true}
