//= require_tree

ready = ->

        $j = jQuery

        bar = $j '#bar'
        logo = $j '#logo'
        viewers = $j "#viewers"
        a = 1
        video = videojs 'my-video'


        endDate = new Date $('#clock').data("content")

        $('#clock').countdown(endDate).on('update.countdown', (event) ->
                format = '%H:%M:%S'

                if event.offset.totalDays > 0
                        format = '%-d day%!d ' + format

                if event.offset.weeks > 0
                        format = '%-w week%!w ' + format

                $(this).html 'Live in ' +  event.strftime(format)
                return

        ).on 'finish.countdown', (event) ->

                $(this).html('LIVE!').parent().addClass('live')
                return
        #if I just put this bit into the get it doesn't work, idk why.
        isOffline = (response) ->
                if  response < 1
                        return "Offline"
                else
                        return response

        getViewerCount = ->
                if not video.paused()
                    params = "?playing=true"
                else
                    params = ""
                count = $.get("/viewers"+params, ->
                    viewers.text isOffline(String(count.responseText))
                )

        $ ->
                setInterval getViewerCount, 10000
                return


        $j ->
                bar.mouseenter ->
        #                 bar.animate {height:500}, 500
                         logo.animate {top:20}, 500

        #         bar.mouseleave ->
        #                 bar.stop true
        #                 bar.animate {height:45}, 500
        $('#theather-mode').click (event) ->
                videoContainer = $('#video-container')
                chatContainer = $('#chat-container')

                if  videoContainer.hasClass('video-theather')
                         $('#snow').addClass('snow') if not $('#snow').hasClass('snow')
                         $('.hide-in-theather').show()
                         videoContainer.removeClass('video-theather').addClass('video-container')
                         chatContainer.removeClass('chat-theather').addClass('chat-container')
                else
                         $('.hide-in-theather').hide()
                         $('#snow').removeClass('snow') if  $('#snow').hasClass('snow')
                         videoContainer.removeClass('video-container').addClass('video-theather')
                         chatContainer.removeClass('chat-container').addClass('chat-theather')
        $(document).keydown (event) ->
        # 27 is the keycode for escape
                if  event.which == 27
                         $('.hide-in-theather').show()
                         $('#snow').addClass('snow') if not $('#snow').hasClass('snow')
                         $('#video-container').removeClass('video-theather').addClass('video-container')
                         $('#chat-container').removeClass('chat-theather').addClass('chat-container')
        $('#snow-toggle').click (event) ->
                if  $('#snow').hasClass('snow')
                         $('#snow').removeClass('snow')
                else
                         $('#snow').addClass('snow')

$(document).ready(ready)
$(document).on('page:load', ready)
