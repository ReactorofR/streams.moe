Vue.filter('time', function(date){
    //pad() and months are found in the app.js file
    //check if date older than 1 day
    var ONE_DAY = 24*60*60*1000;
    if (typeof date === 'string') {
        date = new Date(date);
    }
    if((new Date - date) < ONE_DAY){
        return pad(date.getHours()) + ':' + pad(date.getMinutes()) + ':' + pad(date.getSeconds());
    } else {
        return date.getDate() + ' ' + months[date.getMonth()];
    }
});

var chat = new Vue({
    el: '#chat',

    //TODO Turn emote list into an array
    data: {
        messages: [],
        room: 1,
        chat_box : '',
        userBox: '',
        user: '',
        users: [],
        state: '',
        mode: 'full',
        expandedImage:'',
        emoteList: [
          'reina',
          'eh',
          'dekinai',
          'sharo',
          'sekuhara',
          'sip',
          'kumiko',
          'smugcat',
          'thirst',
          'crab',
          'b1',
          'thinking',
          'geh',
          'freiya',
          'strawberry',
          'panic',
          'thumbs_up',
          'kongeh',
          'sake',
          'hibiki',
          'wave',
          'heart',
          'intense_blush',
          'id_geh',
          'asuka',
          'cry',
          'towelguy',
          'ok_hand',
          'pout',
          'nani',
          'smug',
          'awoo',
          'jazz',
          'confus',
          'fire',
          'rage',
          'Seyana',
          'AzuCheeks',
          'AzuComfy',
          'AzuPeek',
          'AzuTea',
          'AzuToast',
          'ChiiFace',
          'JunTadaima',
          'MioCry',
          'RitsuStop',
          'RitsuThink',
          'Yui',
          'YuiShrug'
      ]
    },

    computed: {
        userCookie: {
            get: function(){
                return document.cookie.replace(/(?:(?:^|.*;\s*)user\s*\=\s*([^;]*).*$)|^.*$/, "$1");
            },
            set: function(name){
                document.cookie = "user="+name+"; expires=Fri, 31 Dec 9999 23:59:59 GMT";
                App.user.anounce(name);
            }
        },
        modeCookie: {
            get: function(){
                return document.cookie.replace(/(?:(?:^|.*;\s*)mode\s*\=\s*([^;]*).*$)|^.*$/, "$1");
            },
            set: function(mode){
                document.cookie = "mode="+mode+"; expires=Fri, 31 Dec 9999 23:59:59 GMT";
            }
        }
    },

    methods: {
        parseRecieved: function(data){
            if(data.type === "messages"){
                this.messages = data.messages;
                this.scrollToBottom();
            } else if (data.type === "message"){
                this.messages.push(data.message);
                this.scrollMessageBox();
                if(data.message.text.indexOf(':awoo:') > -1){
                    awoo();
                }
            } else if (data.type === "info"){
                this.messages.push(data);
                this.scrollMessageBox();
            } else if (data.type === "users"){
                this.users = [];
                this.users = data.users ;
            } else if (data.type === "mention"){
                mentionSound.play();
            }
        },

        ready: function(){
            this.state = "ready";
            if(!this.user && this.userBox){
                this.user = this.userBox;
                this.userCookie = this.user;
            } else {
                window.setTimeout(function(){App.user.anounce(window.chat.user);},500);
            }
            this.$refs.chatBox.focus();
            this.$refs.chatBox.select();
        },

        login: function(){
            if(this.userBox){
                this.ready();
            } else {
                this.userBox = 'Anon' + (Math.floor(Math.random() * 100));
                this.ready();
            }
        },

        options: function(){
            if(this.state != "options"){
                this.state = "options";
                slideInOptions = anime({
	                  targets: "#option-menu",
	                  translateY: '0%',
                    easing: 'easeOutQuad',
	                  elasticity: 0,
                    duration: 300
                });
            } else {
                if(this.userBox !== this.user){
                    this.user = this.userBox;
                    this.userCookie = this.userBox;
                }
                slideOurOptions = anime({
	                  targets: "#option-menu",
	                  translateY: '-100%',
                    easing: 'easeOutQuad',
	                  elasticity: 0,
                    duration: 300,
                    complete: function(anim){
                        chat.state = 'ready';
                    }
                });

                // this.state = 'ready';
            }
        },

        userList: function(){
            if(this.state != "users"){
                slideInUsers = anime({
	                  targets: "#user-list",
	                  translateY: '0%',
                    easing: 'easeOutQuad',
	                  elasticity: 0,
                    duration: 300
                });
                this.state = "users";
            } else {
                slideOurUsers = anime({
	                  targets: "#user-list",
	                  translateY: '-100%',
                    easing: 'easeOutQuad',
	                  elasticity: 0,
                    duration: 300,
                    complete: function(anim){
                        chat.state = 'ready';
                    }

            });

            }
        },

        sendMessage: function(){
            data = {text: this.chat_box, user: this.user, room: this.room};
            if(this.chat_box.includes('@')){
                var re = /(?:^|\W)@(\w+)(?!\w)/g, match, matches = [];
                while ((match = re.exec(this.chat_box))){
                    matches.push(match[1]);
                }
                data.mentions = matches;
            }
            if(this.autoInsertEmote(this.chat_box)){
            } else {
                App.chat.speak(data);
                this.chat_box = '';
                this.scrollMessageBox();
            }
        },

        switchMode: function(mode){
            this.mode = mode;
            this.modeCookie = mode;
            this.scrollMessageBox();
        },

        scrollMessageBox: function(){
            if(!this.lockAutoScroll()){
                var messageBox = this.$refs.messageBox;
                Vue.nextTick(function(){
                    scrollToBottom = anime({
                        targets: '#message-box',
                        scrollTop: messageBox.scrollHeight,
                        easing: 'easeOutCubic',
                        duration: 300,
                        elasticity: '0'
                    });
                },50);
            }
        },

        scrollToBottom: function(){
            var messageBox = this.$refs.messageBox;
            Vue.nextTick(function(){
                scrollToBottom = anime({
                    targets: '#message-box',
                    scrollTop: messageBox.scrollHeight,
                    duration: 500,
                    easing: 'easeOutCubic',
                    elasticity: '0'
                });
            },50);
        },

        markedMessage: function(message){
            //escapeHTML found in welcome.js
            message = escapeHtml(message);
            var marked = message;
            if(message.includes('@')){
                //Matches user mentions
                var reMention = /(?:^|\W)(@\w+)(?!\w)/g, match;
                while ((match = reMention.exec(message))){
                    marked = marked.replace(match[1],'<span class="mention">' + match[1] + '</span>');
                }
            }

            if(message.includes(':')){
                //Matches emotes, for example :sip:
                var reEmote = /(?:^|\W):(\w+):(?!\w)/g, match;
                while((match = reEmote.exec(message))){
                    let index = this.emoteList.indexOf(match[1]);
                    if( -1 != index ){
                        marked = marked.replace(match[0],'<img src="/emotes/' + this.emoteList[index] + '.png" class="emote">');
                    }
                }
            }

            var urlRe = /(http(s)?:\/\/.)?(www\.)?[-a-zA-Z0-9@:%._\+~#=]{2,256}\.[a-z]{2,6}\b([-a-zA-Z0-9@:%_\+.~#?&\/\/=]*)/g;
            while ((match = urlRe.exec(message))){
                if(/jpg|png/.exec(match[0])){
                    marked = marked.replace(match[0],'');
                } else if(match[1]){
                    marked = marked.replace(match[0],'<a target="_blank" href="' + match[0] +  '">' + match[0] + '</a>');
                } else {
                    marked = marked.replace(match[0],'<a target="_blank" href="http://' + match[0] +  '">' + match[0] + '</a>');
                }
            }
            return marked;
        },
        messageImages: function(message){
            message = escapeHtml(message);
            var images = [];
            var urlRe = /(http(s)?:\/\/.)?(www\.)?[-a-zA-Z0-9@:%._\+~#=]{2,256}\.[a-z]{2,6}\b([-a-zA-Z0-9@:%_\+.~#?&\/\/=]*)/g;
            while ((match = urlRe.exec(message))){
                if(/jpg|png/.exec(match[0])){
                    if(match[1]){
                        images.push(match[0]);
                    } else {
                        images.push("http://"+match[0]);
                    }
                }
            }
            return images;
        },

        expandImage: function(image){
            this.state = "expanded";
            this.expandedImage = image;
        },

        emoteListShow: function(input){
            //Checks if chat box has any incomplete emotes like : or :asd
            //but not :asda:
            var reIncompleteEmote = /(:\w+$)|(\s:$)/;
            if(reIncompleteEmote.exec(input)){
                return true;
            } else {
                return false;
            }
        },

        filterEmotes: function(emoteList,input){
            //Displays emotes the user has partially entered
            var reIncompleteEmote = /:(\w+$)/;
            var searchTarget = reIncompleteEmote.exec(input);
            let sortEmotes = function(a,b){
                //emotes to be sorted will always contain the search target somewhere
                if (a.indexOf(searchTarget[1]) < b.indexOf(searchTarget[1])){
                    return 1;
                }
                if (a.indexOf(searchTarget[1]) > b.indexOf(searchTarget[1])){
                    return -1;
                }
                else {
                    return 0;
                }
            };
            if(searchTarget && searchTarget[1]){
                filteredEmotes = [];
                for (emote in emoteList){
                    if(emoteList[emote].includes(searchTarget[1])){
                        filteredEmotes.push(emoteList[emote]);
                    }
                }
                filteredEmotes.sort();
                filteredEmotes.sort(sortEmotes);
                return filteredEmotes;
            } else {
                return emoteList;
            }
        },

        insertEmote: function(emote,input){
            this.chat_box = this.chat_box.replace(/(:\w+$)/,':'+ emote + ': ');
            this.$refs.chatBox.focus();
            this.$refs.chatBox.select();
        },

        autoInsertEmote: function(input){
            //Called in sendMessage, checks if there's an incomplete emote, attempts to complete it
            //If it succeeds message isn't sent
            var reIncompleteEmote = /(:\w+$)/;
            found = reIncompleteEmote.exec(input);
            if(found && found[1]){
                var list = this.filterEmotes(this.emoteList,input);
                if(list.length > 0){
                    this.insertEmote(list.reverse()[0],input);
                    return true;
                }
                return false;
            } else {
                return false;
            }
        },
        lockAutoScroll: function(){
            if(this.state === 'ready'){
                var messageBox = this.$refs.messageBox;
                if((messageBox.scrollHeight - messageBox.scrollTop) >= messageBox.offsetHeight + 300 ){
                    var animation = anime({
                        targets: "#scroll-down-widget",
                        translateY:'0%',
                        easing: 'easeOutCubic',
                        duration: 500
                    });
                    return true;
                } else {
                    this.hideScrollDownWidget();
                    return false;
                }
            } else {
                this.hideScrollDownWidget();
                return false;
            }
        },

        hideScrollDownWidget: function(){
            var animation = anime({
                targets: "#scroll-down-widget",
                translateY:'100%',
                easing: 'easeOutCubic',
                duration: 500
            });
        }
    },

    mounted: function(){
        this.user = this.userCookie;
        this.userBox = this.userCookie;
        if(!this.user){
            this.state = 'login';
        } else {
            this.ready();
        }
        if(this.modeCookie){
            this.mode = this.modeCookie;
        }
    }
});
