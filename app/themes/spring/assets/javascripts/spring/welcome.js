//= require cable
//= require vue
//= require_tree
function pad(n){return n<10 ? '0'+n : n;}

function dhms(t){
    var cd = 24 * 60 * 60 * 1000,
        ch = 60 * 60 * 1000,
        d = Math.floor(t / cd),
        h = Math.floor( (t - d * cd) / ch),
        m = Math.floor( (t - d * cd - h * ch) / 60000),
        s = Math.floor( ((t - d * cd - h * ch) % 60000)/1000);

    if( m === 60 ){
        h++;
        m = 0;
    }
    if( h === 24 ){
        d++;
        h = 0;
    }
    var result = "";
    if ( d > 0 ) {
        result = d + ' Days ';
    }
    result += [pad(h), pad(m), pad(s)].join(':');
    return result;
}


const months = ['Jan','Feb','Mar','Apr','May','Jun',
              'Jul','Aug','Sep','Oct','Nov','Dec'];

var startCountdown = function(elementId){
    var element = document.getElementById(elementId);
    var airDate = new Date(element.dataset.content);
    if(airDate instanceof Date && !isNaN(airDate.valueOf())){
        var now = new Date;
        var timeLeft = airDate - now;
        var data = {element: element, timeLeft: timeLeft};
        countdown(data);
        var interval = window.setInterval(countdown,1000,data);
        data.interval = interval;
    } else {
        element.innerText = 'LIVE IN NYAN:NYAN:NYAN';
    }
};

var countdown = function(data){
    data.timeLeft = data.timeLeft - 1000;
    if(data.timeLeft <= 0){
        data.element.classList.add('live');
        data.element.innerText = 'LIVE!';
        window.clearInterval(data.interval);
    } else {
        data.element.innerText = 'LIVE IN '+dhms(data.timeLeft);
    }
};

const videoContainer = document.getElementById('video-container'),
      chatContainer = document.getElementById('chat-container'),
      contentContainer = document.getElementById('container');
var toggleTheatherMode = function(){
    var elementsToHide = document.getElementsByClassName('hide-in-theather'),
        display;
    if(videoContainer.classList.contains('video-theather')){
        display = 'inline-block';
        contentContainer.classList.add('container');
        videoContainer.classList.add('video-container');
        videoContainer.classList.remove('video-theather');
        chatContainer.classList.add('chat-container');
        chatContainer.classList.remove('chat-theather');
    } else {
        display = 'none';
        contentContainer.classList.remove('container');
        videoContainer.classList.remove('video-container');
        videoContainer.classList.add('video-theather');
        chatContainer.classList.remove('chat-container');
        chatContainer.classList.add('chat-theather');
    }
    for (var i = 0; i < elementsToHide.length; ++i){
        elementsToHide[i].style.display = display;
    }
}

var theatherButton = document.getElementById('theather-mode');

theatherButton.onclick = toggleTheatherMode;
document.onkeydown = function(event){
    if(event.key === 'Escape' && videoContainer.classList.contains('video-theather')){
        toggleTheatherMode();
    }
};

const viewerButton = document.getElementById('viewers');
var getViewerCount = function(){
    var video = videojs('my-video');
    if (!video.paused()){
        var params = "?playing=true";
    } else {
        var params = "";
    }
    fetch('/viewers'+params)
        .then(function(response){
            return response.json();
        })
        .then(function(data){
            if(data > 0){
                viewerButton.innerText = data;
            } else {
                viewerButton.innerText = 'OFFLINE';
            }
        });
};

function escapeHtml(unsafe) {
    return unsafe
         .replace(/&/g, "&amp;")
         .replace(/</g, "&lt;")
         .replace(/>/g, "&gt;")
         .replace(/"/g, "&quot;")
         .replace(/'/g, "&#039;");
};

viewerButton.onclick = getViewerCount;

const mentionSound = new Audio('/chat/mention.mp3');
const awoo = function(){
    let awooSound = new Audio('/chat/awoo.ogg');
    awooSound.play();
    awooSound = undefined;
};

window.onload = function(){
    startCountdown('clock');
    getViewerCount();
    window.setInterval(getViewerCount,30000);
};
