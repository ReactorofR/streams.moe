class WelcomeController < ApplicationController
  theme :theme_resolver

  def index
    shows = Show.where("(start_date <= :now OR end_date <= :now_plus_week)  AND end_date >= :now",
                       {now: Time.now,now_plus_week: Time.now+1.week})
    #Sun = 0  => Sat = 6
    @shows_by_wday = {0=>[],1=>[],2=>[],3=>[],4=>[],5=>[],6=>[]}
    @stream_start_time = 'hello'

    shows.each do |show|
      if show.air_dates.where("date >= :now AND date <= :now_plus_week",
                              {now: Time.now,now_plus_week: Time.now+1.week}).length > 0
        air_dates = show.air_dates.where("date >= :now AND date <= :now_plus_week",
                                         {now: Time.now,now_plus_week: Time.now+1.week})
        air_dates.each do |date|
          @shows_by_wday[date.date.wday].append [show,date.episode]
        end
      else
        #Weeks after 1st episode plus 1
        #Airing anime have a start and end date, movies don't have a start_date
        if show.start_date
          current_episode = ((Time.now - show.start_date)/60/60/24/7).ceil + 1
        else
          current_episode = 0
        end
        #Pretty sure there's a way to make this better looking using || asignment
        if show.air_dates.find_by(episode: current_episode)
          air_date = show.air_dates.find_by(episode: current_episode)
          weekday = (air_date.date.wday if air_date.date < Time.now + 1.week) || nil
        else
          weekday = show.end_date.wday
        end

        @shows_by_wday[weekday].append [show,current_episode] if weekday
      end
    end

    #Sort them by time
    @shows_by_wday.each do |wday|
      wday[1].sort! {|x,y| x[0].end_date.hour <=> y[0].end_date.hour}
    end

    #This is terrible
    #TODO: Rewrite it to something not terrible
    if @shows_by_wday[Time.now.wday].any?
      date = Time.now
      time = @shows_by_wday[Time.now.wday][0][0].end_date
      @stream_start_time = Time.new(date.year,date.month,date.day,
                                    time.hour,time.min).utc.iso8601
    else
      wday = (0..6).to_a.bsearch {|x| @shows_by_wday[x].any? and x > Time.now.wday}
      if wday
        date = Time.now + (wday - Time.now.wday).days
        time = @shows_by_wday[wday][0][0].end_date
        @stream_start_time = Time.new(date.year,date.month,date.day,time.hour,time.min).utc.iso8601
      end
    end
  end

  def video
    render 'video'
  end

  def chat
    render 'chat'
  end

  private
  def theme_resolver
    return 'spring'
  end
end
