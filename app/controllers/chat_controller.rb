class ChatController < ApplicationController

  def create
    room = Room.find(params[:room_id])
    message = room.messages.create(message_params)
    if message.save
      ActionCable.server.broadcast 'chat',
                                   text: message.text,
                                   user: message.user
    end
  end

  def show
    render json: Message.where('room_id = ?',params[:id]).limit(200).order(:created_at)
  end

  def test
    ActionCable.server.broadcast 'chat', text: "hello world"
  end

  private
  def message_params
    params.require(:message).permit(:user,:text)
  end
end
