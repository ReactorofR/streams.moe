class ViewerCountController < ApplicationController
  def index
    if params[:playing]
      if Viewer.exists?(ip: request.remote_ip)
        viewer = Viewer.where('ip = :ip',{ip: request.remote_ip}).first
        viewer.updated_at = Time.now
        viewer.save
      else
        Viewer.create(ip: request.remote_ip)
      end
    end
    render json: Viewer.where('updated_at >= :recent',{recent: Time.now - 30.seconds}).count
  end
end
