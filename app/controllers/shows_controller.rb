class ShowsController < ApplicationController

  http_basic_authenticate_with name: "reactor", password: "animeisfun!"
  
  def index
    @shows = Show.all
  end

  def show
    @show = Show.find(params[:id])
  end

  def new
    @show = Show.new
  end

  def edit
    @show = Show.find(params[:id])
  end

  def create
    @show = Show.new(shows_params)

    if @show.save 
      redirect_to
    else
      render 'new'
    end
  end

  def update
    @show = Show.find(params[:id])

    if @show.update(shows_params)
      redirect_to @show
    else
      render 'edit'
    end
  end

  def destroy
    @show = Show.find(params[:id])
    @show.destroy

    redirect_to shows_path
  end

  private
  def shows_params
    params.require(:show).permit(:title,:start_date,:end_date,
                                 :episode_count,:episodes_watched,
                                 :headliner,:description,:length)
  end
  
end
