class CreateShows < ActiveRecord::Migration[5.0]
  def change
    create_table :shows do |t|
      t.string :title
      t.datetime :start_date
      t.datetime :end_date
      t.integer :episode_count

      t.timestamps
    end
  end
end
