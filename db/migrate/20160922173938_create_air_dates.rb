class CreateAirDates < ActiveRecord::Migration[5.0]
  def change
    create_table :air_dates do |t|
      t.datetime :date
      t.text :comment
      t.references :show, foreign_key: true

      t.timestamps
    end
  end
end
