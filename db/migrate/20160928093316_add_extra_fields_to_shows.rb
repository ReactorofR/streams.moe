class AddExtraFieldsToShows < ActiveRecord::Migration[5.0]
  def change
    add_column :shows, :length, :integer
    add_column :shows, :episodes_watched, :integer
    add_column :shows, :description, :text
    add_column :shows, :headliner, :boolean
    
  end
end
