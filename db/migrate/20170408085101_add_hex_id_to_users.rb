class AddHexIdToUsers < ActiveRecord::Migration[5.0]
  def change
    add_column :users, :hex_id, :string
  end
end
