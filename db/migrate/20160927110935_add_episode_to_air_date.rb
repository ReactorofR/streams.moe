class AddEpisodeToAirDate < ActiveRecord::Migration[5.0]
  def change
    add_column :air_dates, :episode, :integer
  end
end
